module gitlab.com/rahulvramesh/go-reverse-proxy

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.6.1 // indirect
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	golang.org/x/net v0.0.0-20210510120150-4163338589ed // indirect
)
