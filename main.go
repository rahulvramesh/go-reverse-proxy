package main

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"time"

	br "github.com/dsnet/compress/brotli"
	"github.com/gorilla/mux"
)

func main() {

	r := mux.NewRouter().SkipClean(true).UseEncodedPath()

	remote, err := url.Parse("https://notion.so")
	if err != nil {
		panic(err)
	}

	proxy := httputil.NewSingleHostReverseProxy(remote)

	r.PathPrefix("/").HandlerFunc(handler(proxy))

	srv := &http.Server{
		Handler: r,
		Addr:    ":80",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())

}

type transport struct {
	http.RoundTripper
}

func (t *transport) RoundTrip(req *http.Request) (resp *http.Response, err error) {

	// Check For APP File

	fmt.Println("Inside the transport")
	fmt.Println(req.URL.Opaque)

	resp, err = t.RoundTripper.RoundTrip(req)
	if err != nil {
		return nil, err
	}

	var reader io.ReadCloser
	fmt.Println(resp.Header.Get("Content-Encoding"))
	switch resp.Header.Get("Content-Encoding") {
	case "gzip":
		reader, err = gzip.NewReader(resp.Body)
		defer reader.Close()
	case "br":
		reader, err = br.NewReader(resp.Body, &br.ReaderConfig{})
		defer reader.Close()
	default:
		reader = resp.Body
	}

	b, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}

	err = resp.Body.Close()
	if err != nil {
		return nil, err
	}

	b = bytes.Replace(b, []byte("www.notion.so"), []byte("rahulvramesh.me"), -1)
	b = bytes.Replace(b, []byte("notion.so"), []byte("rahulvramesh.me"), -1)

	// Replace HTMl

	//z := html.NewTokenizer(body)

	// fmt.Println(resp.Header.Get("Content-Type"))

	const headerInject = `
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,300&display=swap" rel="stylesheet">
     <style>* { font-family: 'Roboto', sans-serif !important; }</style>
      <style>
        div.notion-topbar > div > div:nth-child(3) { display: none !important; }
        div.notion-topbar > div > div:nth-child(4) { display: none !important; }
        div.notion-topbar > div > div:nth-child(5) { display: none !important; }
        div.notion-topbar > div > div:nth-child(6) { display: none !important; }
        div.notion-topbar-mobile > div:nth-child(3) { display: none !important; }
        div.notion-topbar-mobile > div:nth-child(4) { display: none !important; }
        div.notion-topbar > div > div:nth-child(1n).toggle-mode { display: block !important; }
        div.notion-topbar-mobile > div:nth-child(1n).toggle-mode { display: block !important; }
      </style>
      </head>
    `

	const bodyInject = `
    <script>
      const SLUG_TO_PAGE = {};
      const PAGE_TO_SLUG = {};
      const slugs = [];
      const pages = [];
      const el = document.createElement('div');
      let redirected = false;
      Object.keys(SLUG_TO_PAGE).forEach(slug => {
        const page = SLUG_TO_PAGE[slug];
        slugs.push(slug);
        pages.push(page);
        PAGE_TO_SLUG[page] = slug;
      });
      function getPage() {
        return location.pathname.slice(-32);
      }
      function getSlug() {
        return location.pathname.slice(1);
      }
      function updateSlug() {
        const slug = PAGE_TO_SLUG[getPage()];
        if (slug != null) {
          history.replaceState(history.state, '', '/' + slug);
        }
      }
      function onDark() {
        el.innerHTML = '<div title="Change to Light Mode" style="margin-left: auto; margin-right: 14px; min-width: 0px;"><div role="button" tabindex="0" style="user-select: none; transition: background 120ms ease-in 0s; cursor: pointer; border-radius: 44px;"><div style="display: flex; flex-shrink: 0; height: 14px; width: 26px; border-radius: 44px; padding: 2px; box-sizing: content-box; background: rgb(46, 170, 220); transition: background 200ms ease 0s, box-shadow 200ms ease 0s;"><div style="width: 14px; height: 14px; border-radius: 44px; background: white; transition: transform 200ms ease-out 0s, background 200ms ease-out 0s; transform: translateX(12px) translateY(0px);"></div></div></div></div>';
        document.body.classList.add('dark');
        __console.environment.ThemeStore.setState({ mode: 'dark' });
      };
      function onLight() {
        el.innerHTML = '<div title="Change to Dark Mode" style="margin-left: auto; margin-right: 14px; min-width: 0px;"><div role="button" tabindex="0" style="user-select: none; transition: background 120ms ease-in 0s; cursor: pointer; border-radius: 44px;"><div style="display: flex; flex-shrink: 0; height: 14px; width: 26px; border-radius: 44px; padding: 2px; box-sizing: content-box; background: rgba(135, 131, 120, 0.3); transition: background 200ms ease 0s, box-shadow 200ms ease 0s;"><div style="width: 14px; height: 14px; border-radius: 44px; background: white; transition: transform 200ms ease-out 0s, background 200ms ease-out 0s; transform: translateX(0px) translateY(0px);"></div></div></div></div>';
        document.body.classList.remove('dark');
        __console.environment.ThemeStore.setState({ mode: 'light' });
      }
      function toggle() {
        if (document.body.classList.contains('dark')) {
          onLight();
        } else {
          onDark();
        }
      }
      function addDarkModeButton(device) {
        const nav = device === 'web' ? document.querySelector('.notion-topbar').firstChild : document.querySelector('.notion-topbar-mobile');
        el.className = 'toggle-mode';
        el.addEventListener('click', toggle);
        nav.appendChild(el);
        // enable smart dark mode based on user-preference
        if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
            onDark();
        } else {
            onLight();
        }
        // try to detect if user-preference change
        window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', e => {
            toggle();
        });
      }
      const observer = new MutationObserver(function() {
        if (redirected) return;
        const nav = document.querySelector('.notion-topbar');
        const mobileNav = document.querySelector('.notion-topbar-mobile');
        if (nav && nav.firstChild && nav.firstChild.firstChild
          || mobileNav && mobileNav.firstChild) {
          redirected = true;
          updateSlug();
          addDarkModeButton(nav ? 'web' : 'mobile');
          const onpopstate = window.onpopstate;
          window.onpopstate = function() {
            if (slugs.includes(getSlug())) {
              const page = SLUG_TO_PAGE[getSlug()];
              if (page) {
                history.replaceState(history.state, 'bypass', '/' + page);
              }
            }
            onpopstate.apply(this, [].slice.call(arguments));
            updateSlug();
          };
        }
      });
      observer.observe(document.querySelector('#notion-app'), {
        childList: true,
        subtree: true,
      });
      const replaceState = window.history.replaceState;
      window.history.replaceState = function(state) {
        if (arguments[1] !== 'bypass' && slugs.includes(getSlug())) return;
        return replaceState.apply(window.history, arguments);
      };
      const pushState = window.history.pushState;
      window.history.pushState = function(state) {
        const dest = new URL(location.protocol + location.host + arguments[2]);
        const id = dest.pathname.slice(-32);
        if (pages.includes(id)) {
          arguments[2] = '/' + PAGE_TO_SLUG[id];
        }
        return pushState.apply(window.history, arguments);
      };
      const open = window.XMLHttpRequest.prototype.open;
      window.XMLHttpRequest.prototype.open = function() {
        arguments[1] = arguments[1].replace('rahulvramesh.me', 'www.notion.so');
        return open.apply(this, [].slice.call(arguments));
      };
    </script>
    </body>
    `

	if resp.Header.Get("Content-Type") == "text/html; charset=utf-8" {
		b = bytes.Replace(b, []byte("</head>"), []byte(headerInject), -1)
		b = bytes.Replace(b, []byte("</body>"), []byte(bodyInject), -1)
	}

	body := ioutil.NopCloser(bytes.NewReader(b))

	resp.Body = body
	resp.ContentLength = int64(len(b))

	resp.Header.Set("Accept-Charset", "utf-8")
	resp.Header.Set("Content-Length", strconv.Itoa(len(b)))
	//resp.Header.Set("Content-Encoding", resp.Header.Get("Accept-Encoding"))
	resp.Header.Del("Content-Encoding")
	resp.Header.Del("Content-Security-Policy")
	resp.Header.Del("X-Content-Security-Policy")
	return resp, nil
}

func handler(p *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		log.Println(r.URL.EscapedPath())

		tr := http.DefaultTransport

		p.Transport = &transport{tr}

		p.Director = func(req *http.Request) {
			req.URL.Scheme = "https"
			req.URL.Host = "www.notion.so"
			req.Host = "www.notion.so"

			req.Header.Set("Referer", "https://notion.so")
			req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36")
			req.Header.Set("X-Real-IP", r.Header.Get("X-Real-IP"))

			req.Header.Set("Accept-Encoding", r.Header.Get("Accept-Encoding"))
			req.Header.Set("Accept-Charset", r.Header.Get("utf-8"))
			req.Header.Set("Accept-Language", "en-US,en;q=0.9")

			req.Header.Set("X-Forwarded-For", r.Header.Get("X-Forwarded-For"))
		}

		p.ServeHTTP(w, r)
	}
}
